
package com.example.Tienda_Virtual.Logica;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.jdbc.core.JdbcTemplate;

@Entity
@Table(name="cliente")

public class Clientes implements Serializable {
//Paquete que ejecuta las consultas
    transient JdbcTemplate jdbcTemplate;//Palabra clave en java para serealizar .
// Sirven para demarcar el carácter temporal o transitorio de dicha variable , no siempre se tendrát
// acceso al valor de la variable .          


//atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_cliente")
    private int Id_Cliente;
    @Column(name="contraseña")
    private String Contraseña;  
    @Column(name="correo")
    private String Correo;
    @Column(name="direccion")
    private String Direccion;
    @Column(name="metodo_Pago")
    private String Metodo_Pago;


    public Clientes(int Id_Cliente,String Correo, String Direccion, String Contraseña,String Metodo_Pago ) {
        this.Id_Cliente = Id_Cliente;
        this.Correo = Correo;
        this.Direccion = Direccion;
        this.Contraseña = Contraseña;
        this.Metodo_Pago = Metodo_Pago;
    }

    public Clientes() {
    }

    public int getId_Cliente() {
        return Id_Cliente;
    }

    public void setId_Cliente(int Id_Cliente) {
        this.Id_Cliente = Id_Cliente;
    }
    
    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }
    
    
    public String getMetodo_Pago() {
        return Metodo_Pago;
    }

    public void setMetodo_Pago(String Metodo_Pago) {
        this.Metodo_Pago = Metodo_Pago;
    }

 	


    
    //Crud = Create (c)
    public String guardar ( ) throws ClassNotFoundException, SQLException{
    String sql = " INSERT INTO Cliente(Id_Cliente,Correo,Direccion,Contraseña,Metodo_Pago)VALUES(?,?,?,?)";
    return sql ;              
}
    //Crud = Read (r)
    public boolean consultar() throws ClassNotFoundException,SQLException{
    String sql = "SELECT Id_Cliente,Correo,Direccion,Contraseña,Metodo_Pago FROM Cliente WHERE Id_Cliente" ;
    List<Clientes> clientes= jdbcTemplate.query (sql,(rs, rowNum)
        -> new Clientes(
                rs.getInt("Id_Cliente"),
                rs.getString("Correo"),
                rs.getString("Direccion"),
                rs.getString("Contraseña"),
                rs.getString("Metodo_Pago")

        ),
        new Object[]{this.getId_Cliente()});
        if (clientes != null && clientes.size()>0){
            this.setId_Cliente(clientes.get(0).getId_Cliente());
            this.setCorreo(clientes.get(0).getCorreo());
            this.setDireccion(clientes.get(0).getDireccion());
            this.setContraseña(clientes.get(0).getContraseña());
            this.setMetodo_Pago(clientes.get(0).getMetodo_Pago());
            return true;
        }
        else{
            return false;
        }
    }
    // CRUD = Update (u)
public String actualizar ( ) throws ClassNotFoundException , SQLException {
    String sql= " UPDATE Cliente SET Correo = ? , Direccion = ? , Contraseña = ?, Metodo_Pago = ? WHERE id_cliente = ? " ;
     return sql ;
}

// CRUD = Delete (D)
public boolean borrar ( ) throws ClassNotFoundException , SQLException {
    String sql = " DELETE FROM cuenta WHERE id_cliente = ? " ;
    Connection c = jdbcTemplate.getDataSource().getConnection ( ) ;
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1,this.getId_Cliente());
    ps.execute();
    ps.close();
    return true;
}                                               
}


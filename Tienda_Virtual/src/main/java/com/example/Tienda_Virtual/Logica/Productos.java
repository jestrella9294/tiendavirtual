package com.example.Tienda_Virtual.Logica;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.jdbc.core.JdbcTemplate;

@Entity
@Table(name="Productos")

public class Productos implements Serializable {
//Paquete que ejecuta las consultas
    transient JdbcTemplate jdbcTemplate;//Palabra clave en java para serealizar .
// Sirven para demarcar el carácter temporal o transitorio de dicha variable , no siempre se tendrát
// acceso al valor de la variable .          




//atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_productos")
    private int Id_productos;
    @Column(name="producto")
    private String Producto ;
    @Column(name="precio_producto")
    private float precio_producto;
    @Column(name="descuento")
    private float descuento ;
    @Column(name="disponibilidad")
    private String Disponibilidad ;
    @Column(name="producto_recien")
    private boolean producto_recien ;

    public Productos (int Id_productos, String Producto, float precio_producto, float descuento, String Disponibilidad, boolean producto_recien) {
        this.Id_productos = Id_productos;
        this.Producto = Producto;
        this.precio_producto = precio_producto;
        this.descuento = descuento;
        this.Disponibilidad = Disponibilidad;
        this.producto_recien = producto_recien;
    }
    
    public int getId_productos() {
        return Id_productos;
    }

    public void setId_productos(int Id_productos) {
        this.Id_productos = Id_productos;
    }

    public String getProducto() {
        return Producto;
    }

    public void setProducto(String Producto) {
        this.Producto = Producto;
    }

    public float getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(float precio_producto) {
        this.precio_producto = precio_producto;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public String getDisponibilidad() {
        return Disponibilidad;
    }

    public void setDisponibilidad(String Disponibilidad) {
        this.Disponibilidad = Disponibilidad;
    }

    public boolean isProducto_recien() {
        return producto_recien;
    }

    
    public void setProducto_recien(boolean producto_recien) {
        this.producto_recien = producto_recien;
    }
    
    //Crud = Create (c)
    public String guardar ( ) throws ClassNotFoundException, SQLException{
    String sql = " INSERT INTO Productos(Id_productos,Producto,precio_producto,descuento,Disponibilidad,producto_recien)VALUES(?,?,?,?)";
    return sql ;              
}
    //Crud = Read (r)
    public boolean consultar() throws ClassNotFoundException,SQLException{
    String sql = "SELECT Id_productos,id_administrador,precio_producto,descuento,Disponibilidad,producto_recien FROM Productos WHERE Id_Productos" ;
    List<Productos> Productos= jdbcTemplate.query (sql,(rs, rowNum)
        -> new Productos(
                rs.getInt("Id_productos"),
                rs.getString("Producto"),
                rs.getInt("precio_producto"),
                rs.getInt("descuento"),
                rs.getString("Disponibilidad"),
                rs.getBoolean("producto_recien")
        ),
        new Object[]{this.getId_productos()});
        if (Productos!= null && Productos.size()>0){
            this.setId_productos(Productos.get(0).getId_productos());
            this.setProducto(Productos.get(0).getProducto());
            this.setPrecio_producto(Productos.get(0).getPrecio_producto());
            this.setDescuento(Productos.get(0).getDescuento());
            this.setDisponibilidad(Productos.get(0).getDisponibilidad());
            this.setProducto_recien(Productos.get(0).isProducto_recien());
            
            return true;
        }
        else{
            return false;
        }
    }
    // CRUD = Update (u)
public String actualizar ( ) throws ClassNotFoundException , SQLException {
    String sql= " UPDATE Productos SET Id_productos=?,id_administrador=?,precio_producto=?,descuento=?,disponibilidad_en_stock=?,producto_recien=?";
     return sql ;
}

// CRUD = Delete (D)
public boolean borrar ( ) throws ClassNotFoundException , SQLException {
    String sql = " DELETE FROM producots WHERE Id_productos = ? " ;
    Connection c = jdbcTemplate.getDataSource().getConnection ( ) ;
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1,this.getId_productos());
    ps.execute();
    ps.close();
    return true;
}                                               

}



package com.example.Tienda_Virtual.Logica;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.jdbc.core.JdbcTemplate;

@Entity
@Table(name="usuarios")

public class Usuarios implements Serializable {
//Paquete que ejecuta las consultas
    transient JdbcTemplate jdbcTemplate;//Palabra clave en java para serealizar .
// Sirven para demarcar el carácter temporal o transitorio de dicha variable , no siempre se tendrát
// acceso al valor de la variable .          




//atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_isuarios")
    private int Id_Usuarios;
    @Column(name="nombre")
    private String Nombre;
    @Column(name="tipo_doc")
    private String Tipo_Doc;
    @Column(name="nro_doc")
    private int Nro_Doc;
    @Column(name="categoria")
    private String Categoria;

    public Usuarios(int Id_Usuarios,String Nombre, String Tipo_Doc, int Nro_Doc, String Categoria) {
        this.Id_Usuarios = Id_Usuarios;
        this.Nombre = Nombre;
        this.Tipo_Doc = Tipo_Doc;
        this.Nro_Doc = Nro_Doc;
        this.Categoria = Categoria;
    }

    public Usuarios() {
    }

    public int getId_Usuarios() {
        return Id_Usuarios;
    }

    public void setId_Usuarios(int Id_Usuarios) {
        this.Id_Usuarios = Id_Usuarios;
    }
    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getTipo_Doc() {
        return Tipo_Doc;
    }

    public void setTipo_Doc(String Tipo_Doc) {
        this.Tipo_Doc = Tipo_Doc;
    }

    public int getNro_Doc() {
        return Nro_Doc;
    }

    public void setNro_Doc(int Nro_Doc) {
        this.Nro_Doc = Nro_Doc;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }
    
    //Crud = Create (c)
    public String guardar ( ) throws ClassNotFoundException, SQLException{
    String sql = " INSERT INTO Usuarios(Id_Usuarios,Nombre,Tipo_Doc,Nro_Doc,Categoria)VALUES(?,?,?,?)";
    return sql ;              
}
    //Crud = Read (r)
    public boolean consultar() throws ClassNotFoundException,SQLException{
    String sql = "SELECT Id_Usuarios,Nombre,Tipo_Doc,Nro_Doc,Categoria FROM Usuarios WHERE Nombre" ;
    List<Usuarios> usuarios= jdbcTemplate.query (sql,(rs, rowNum)
        -> new Usuarios(
                rs.getInt("Id_Usuarios"),
                rs.getString("Nombre"),
                rs.getString("Tipo_Doc"),
                rs.getInt("Nro_Doc"),
                rs.getString("Categoria")
        ),
        new Object[]{this.getNombre()});
        if (usuarios != null && usuarios.size()>0){
            this.setId_Usuarios(usuarios.get(0).getId_Usuarios());
            this.setNombre(usuarios.get(0).getNombre());
            this.setTipo_Doc(usuarios.get(0).getTipo_Doc());
            this.setNro_Doc(usuarios.get(0).getNro_Doc());
            this.setCategoria(usuarios.get(0).getCategoria());
            return true;
        }
        else{
            return false;
        }
    }
    // CRUD = Update (u)
public String actualizar ( ) throws ClassNotFoundException , SQLException {
    String sql= " UPDATE Usuarios SET Nombre = ? , Tipo_Doc = ? , Nro_Doc = ?, categoria = ? WHERE id_usuarios = ? " ;
     return sql ;
}

// CRUD = Delete (D)
public boolean borrar ( ) throws ClassNotFoundException , SQLException {
    String sql = " DELETE FROM usuarios WHERE id_usuarios = ? " ;
    Connection c = jdbcTemplate.getDataSource().getConnection ( ) ;
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1,this.getId_Usuarios());
    ps.execute();
    ps.close();
    return true;
}                                               

}    

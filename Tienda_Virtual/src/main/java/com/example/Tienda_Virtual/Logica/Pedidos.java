package com.example.Tienda_Virtual.Logica;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.jdbc.core.JdbcTemplate;

@Entity
@Table(name="Pedidos")

public class Pedidos implements Serializable {
//Paquete que ejecuta las consultas
    transient JdbcTemplate jdbcTemplate;//Palabra clave en java para serealizar .
// Sirven para demarcar el carácter temporal o transitorio de dicha variable , no siempre se tendrát
// acceso al valor de la variable .          




//atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_Pedidos")
    private int Id_Pedidos;
    @Column(name="id_Producto")
    private int Id_Producto;
    @Column(name="Nro_Pago")
    private int Nro_Pago;
    @Column(name="Entrega")
    private String Entrega;
    @Column(name="Estado_de_Entrega")
    private String Estado_de_Entrega;
    @Column(name="Registro_Metodo_de_Pago")
    private String Registro_Metodo_de_Pago;
    @Column(name="numero_pedido")
    private int Numero_Pedido;
    

    public Pedidos(int Id_Pedidos, int Id_Producto, int Nro_Pago, String Entrega, String Estado_de_Entrega,String Registro_metodo_de_Pago, int Numero_Pedido ) {
        this.Id_Pedidos = Id_Pedidos;
        this.Id_Producto = Id_Producto;
        this.Nro_Pago = Nro_Pago;
        this.Entrega = Entrega;
        this.Estado_de_Entrega = Estado_de_Entrega;
        this.Registro_Metodo_de_Pago = Registro_metodo_de_Pago;
        this.Numero_Pedido = Numero_Pedido;
    }

    public Pedidos() {
    }

    public int getId_Pedidos() {
        return Id_Pedidos;
    }

    public void setId_Pedidos(int Id_Pedidos) {
        this.Id_Pedidos = Id_Pedidos;
    }
    
    public int getId_Producto() {
        return Id_Producto;
    }

    public void setId_Producto(int Id_Producto) {
        this.Id_Producto = Id_Producto;
    }

    public void setId_Productos(int Id_Producto) {
        this.Id_Producto = Id_Producto;
    }

    public int getNro_Pago() {
        return Nro_Pago;
    }

    public void setNro_Pago(int Nro_Pago) {
        this.Nro_Pago = Nro_Pago;
    }

    public String getEntrega() {
        return Entrega;
    }

    public void setEntrega(String Entrega) {
        this.Entrega = Entrega;
    }

    public String getEstado_de_Entrega() {
        return Estado_de_Entrega;
    }

    public void setEstado_de_Entrega(String Estado_de_Entrega) {
        this.Estado_de_Entrega = Estado_de_Entrega;
    }
    
    public String getRegistro_Metodo_de_Pago() {
        return Registro_Metodo_de_Pago;
    }
    
    public void setRegistro_Metodo_de_Pago(String Registro_Metodo_de_Pago) {
        this.Registro_Metodo_de_Pago = Registro_Metodo_de_Pago;
    }
    
    public int getNumero_Pedido() {
        return Numero_Pedido;
    }

    public void setNumero_Pedido(int Numero_Pedido) {
        this.Numero_Pedido = Numero_Pedido;
    }

    
    //Crud = Create (c)
    public String guardar ( ) throws ClassNotFoundException, SQLException{
    String sql = " INSERT INTO Pedidos(Id_Pedidos,Id_producto,Nro_pago,Entrega,Estado_de_Entrega,Registro_Metodo_de_Pago,Numero_Pedido)VALUES(?,?,?,?)";
    return sql ;              
}
    //Crud = Read (r)
    public boolean consultar() throws ClassNotFoundException,SQLException{
    String sql = "SELECT Pedidos,Id_Pedidos,Id_Producto,Nro_pago,Entrega,Estado_de_Entrega,Registro_Metodo_de_Pago,Numero_Pedido FROM Pedidos WHERE Id_Pedidos" ;
    List<Pedidos> pedido= jdbcTemplate.query (sql,(rs, rowNum)
        -> new Pedidos(
                rs.getInt("Id_Pedidos"),
                rs.getInt("Id_Producto"),
                rs.getInt("Nro_pago"),
                rs.getString("Entrega"),
                rs.getString("Estado_de_Entrega"),
                rs.getString("Metodo_de_Pago"),
                rs.getInt("Numero_Pedido")
        ),
        new Object[]{this.getPedidos()});
        if (pedido != null && pedido.size()>0){
            this.setId_Pedidos(pedido.get(0).getId_Pedidos());
            this.setId_Producto(pedido.get(0).getId_Producto());
            this.setNro_Pago(pedido.get(0).getNro_Pago());
            this.setEntrega(pedido.get(0).getEntrega());
            this.setEstado_de_Entrega(pedido.get(0).getEstado_de_Entrega());
            this.setRegistro_Metodo_de_Pago(pedido.get(0).getRegistro_Metodo_de_Pago());
            this.setNumero_Pedido(pedido.get(0).getNumero_Pedido());
            return true;
        }
        else{
            return false;
        }
    }
    // CRUD = Update (u)
public String actualizar ( ) throws ClassNotFoundException , SQLException {
    String sql= " UPDATE Pedidos SET Producto = ? , Nro_Pago = ? , Entrega = ?, Estado_de_Entrega = ?, Registro_Metodo_de_Pago = ?, Numero_Pedido = ? WHERE id_pedidos = ? " ;
     return sql ;
}

// CRUD = Delete (D)
public boolean borrar ( ) throws ClassNotFoundException , SQLException {
    String sql = " DELETE FROM Pedidos WHERE id_pedidos = ? " ;
    Connection c = jdbcTemplate.getDataSource().getConnection ( ) ;
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1,this.getId_Pedidos());
    ps.execute();
    ps.close();
    return true;
}                                               

    private Object getPedidos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}    




